<?php
define('DB_HOST', 'localhost');
define('DB_USER', 'homestead');
define('DB_PASS', 'secret');
define('DB_NAME', 'widget_corp');

$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

if (mysqli_connect_errno()) {
    die(
        'database connection failed: '
        . mysqli_connect_error()
        . ' ('
        . mysqli_connect_errno()
        . ')'
    );
}
