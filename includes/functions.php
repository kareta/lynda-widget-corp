<?php

function redirect_to($new_location) {
    header("Location: " . $new_location);
    exit;
}

function confirmQuery($result_set) {
    if (!$result_set) {
//        die('database query failed.');
    }
}

function formErrors(&$errors=array()) {
    $output = "";
    if (!empty($errors)) {
        $output .= "<div class \"error\">";
        $output .= "Please fix the following errors:";
        $output .= "<ul>";
        foreach ($errors as $key => $error) {
            $output .= "<li>{$error}</li>";
        }
        $output .= "</ul>";
        $output .= "</div>";
    }
    return $output;
}

function getSubjectsCount($connection) {
    $subjectsQuery = 'SELECT COUNT(*) as subjects_count FROM subjects ';
    $subjectsResult = mysqli_query($connection, $subjectsQuery);
    confirmQuery($subjectsResult);
    $raw = mysqli_fetch_assoc($subjectsResult);
    return $raw["subjects_count"];
}

function findAllSubjects($connection) {
    $subjectsQuery = 'SELECT * FROM subjects ';
    $subjectsQuery .= 'WHERE visible = 1 ';
    $subjectsQuery .= 'ORDER BY position ASC ';
    $subjectsResult = mysqli_query($connection, $subjectsQuery);
    confirmQuery($subjectsResult);
    return $subjectsResult;
}

function findPagesForSubject($connection, $subjectId) {
    $safeSubjectId = mysqli_real_escape_string($connection, $subjectId);
    $pagesQuery = 'SELECT * FROM pages ';
    $pagesQuery .= 'WHERE visible = 1 ';
    $pagesQuery .= "AND subject_id = $safeSubjectId ";
    $pagesQuery .= 'ORDER BY position ASC ';
    $pagesResult = mysqli_query($connection, $pagesQuery);
    confirmQuery($pagesResult);
    return $pagesResult;
}

function findById($connection, $tableName, $id) {
    $safeId = mysqli_real_escape_string($connection, $id);
    $safeTableName = mysqli_real_escape_string($connection, $tableName);
    $query = "SELECT * FROM $safeTableName ";
    $query .= "WHERE id = $safeId ";
    $query .= "LIMIT 1";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);
    $raw = mysqli_fetch_assoc($result);
    return $raw;
}

function findSubjectById($connection, $subjectId) {
    return findById($connection, 'subjects', $subjectId);
}

function findPageById($connection, $pageId) {
    return findById($connection, 'pages', $pageId);
}

function findSelectedPage($connection, &$currentSubject, &$currentPage) {
    if (isset($_GET['subject'])) {
        $currentSubject = findSubjectById($connection, $_GET['subject']);
    } elseif (isset($_GET['page'])) {
        $currentPage = findPageById($connection, $_GET['page']);
    }
}

function renderNavigation($connection, $currentSubject, $currentPage) {
    $output = '';
    $subjectsResult = findAllSubjects($connection);
    while ($subject = mysqli_fetch_assoc($subjectsResult)) {
        $output .= '<li ';
        if ($currentSubject['id']  == $subject['id']) {
            $output .= 'class="selected" ';
        }
        $output .= '>';
        $output .= "<a href=\"manage_content.php?subject={$subject['id']}\">";
        $output .= "{$subject['menu_name']}";
        $output .= "</a>";
        $pagesResult = findPagesForSubject($connection, $subject['id']);
        $output .= "<ul class=\"pages\">";
        while ($page = mysqli_fetch_assoc($pagesResult)) {
            $output .= '<li ';
            if ($currentPage['id'] == $page['id']) {
                $output .= 'class="selected" ';
            }
            $output .= '>';
            $output .= "<a href=\"manage_content.php?page={$page['id']}\">";
            $output .= "{$page['menu_name']}";
            $output .= "</a>";
            $output .= "</li>";
        }
        mysqli_free_result($pagesResult);
        $output .= "</ul>";
        $output .= "</li>";
    }
    mysqli_free_result($subjectsResult);
    return $output;
}

