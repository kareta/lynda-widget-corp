<?php

$errors = array();

function fieldNameAsText($fieldName) {
    $fieldName = str_replace("_", " ", $fieldName);
    $fieldName = ucfirst($fieldName);
    return $fieldName;
}

function hasPresence($value) {
    return isset($value) && $value !== "";
}

function validatePresences(&$errors, $requiredFields) {
    foreach ($requiredFields as $field) {
        $value = trim($_POST[$field]);
        if (!hasPresence($value)) {
            $_SESSION['really'] = "really";
            $errors[$field] = ucfirst($field) . " can't be blank";
        }
    }
}

function hasMaxLength($value, $max) {
    return strlen($value) <= $max;
}

function hasInclusionIn($value, $set) {
    return in_array($value, $set);
}

function validateMaxLength(&$errors, $fieldsWithMaxLength) {
    foreach($fieldsWithMaxLength as $field => $max) {
        $value = trim($_POST[$field]);
        if (!hasMaxLength($value, $max)) {
            $errors[$field] = ucfirst($field) . " is too long";
        }
    }
}

