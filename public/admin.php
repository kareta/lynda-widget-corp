<?php include('../includes/layouts/header.php')?>

<main>
    <nav>
        &nbsp;
    </nav>
    <div id="page">
        <h2>Admin Menu</h2>
        <p>Welcome to the admin area.</p>
        <ul>
            <li><a href="manage_content.php">Manage website content</a></li>
            <li><a href="manage_admins.php">Manage admins</a></li>
            <li><a href="logout.php">logout</a></li>
        </ul>
    </div>
</main>

<?php include('../includes/layouts/footer.php')?>
