<?php require_once('../includes/session.php'); ?>
<?php require_once('../includes/db_connection.php'); ?>
<?php require_once('../includes/functions.php'); ?>
<?php require_once('../includes/validation_functions.php'); ?>

<?php

if (isset($_POST['submit'])) {

    $menuName = $_POST['menu_name'];
    $position = (int) $_POST['position'];
    $visible = (int) $_POST['visible'];

    $menuName = mysqli_real_escape_string($connection, $menuName);
    $position -= 1;

    $requiredFields = array("menu_name", "position", "visible");
    validatePresences($errors, $requiredFields);

    $fieldsWithMaxLength = array("menu_name" => 30);
    validateMaxLength($errors, $fieldsWithMaxLength);


    if (!empty($errors)) {
        $_SESSION["errors"] = $errors;
        redirect_to('new_subject.php');
    }

    $query = "INSERT INTO subjects (";
    $query .= "menu_name, position, visible";
    $query .= ") VALUES(";
    $query .= "'{$menuName}', {$position}, {$visible}";
    $query .= ")";
    $result = mysqli_query($connection, $query);
    $message = '';

    if ($result) {
        $_SESSION['message'] = 'Subject created.';
        redirect_to('manage_content.php');
    } else {
        $_SESSION['message'] = 'Subject creation failed.';
        redirect_to('new_subject.php');
    }

} else {
    redirect_to('new_subject.php');
}

if (isset($connection)) {
    mysqli_close($connection);
}



