<?php require_once('../includes/session.php'); ?>
<?php require_once('../includes/db_connection.php'); ?>
<?php require_once('../includes/functions.php'); ?>
<?php require_once('../includes/validation_functions.php'); ?>

<?php
error_reporting(E_STRICT);
$currentSubject = null;
$currentPage = null;
findSelectedPage($connection, $currentSubject, $currentPage);
if (!$currentSubject) {
    redirect_to('manage_content.php');
}


if (isset($_POST['submit'])) {

    $id = $currentSubject['id'];
    $menuName = $_POST['menu_name'];
    $position = (int) $_POST['position'];
    $visible = (int) $_POST['visible'];

    $menuName = mysqli_real_escape_string($connection, $menuName);
    $position -= 1;

    $requiredFields = array("menu_name", "position", "visible");
    validatePresences($errors, $requiredFields);

    $fieldsWithMaxLength = array("menu_name" => 30);
    validateMaxLength($errors, $fieldsWithMaxLength);


    if (empty($errors)) {
        $query = "UPDATE subjects SET ";
        $query .= "menu_name = '{$menuName}', ";
        $query .= "position = {$position}, ";
        $query .= "visible = {$visible} ";
        $query .= "WHERE id = {$id} ";
        $query .= "LIMIT 1";
        $result = mysqli_query($connection, $query);
        $message = '';

        if ($result) {
            $_SESSION['message'] = 'Subject changed.';
            redirect_to("manage_content.php?subject={$currentSubject['id']}");
        } else {
            $_SESSION['message'] = 'Failded to edit subject.';
            redirect_to('edit_subject.php');
        }
    }
}
?>

<?php include('../includes/layouts/header.php'); ?>

<main>
    <nav class="subjects">
        <?php echo renderNavigation($connection, $currentSubject, $currentPage); ?>
    </nav>
    <div id="page">
        <?php echo message(); ?>
        <?php echo formErrors(errors()); ?>

        <h2>Edit subject: <?php echo $currentSubject['menu_name']; ?></h2>
        <form action="edit_subject.php?subject=<?php echo $currentSubject['id']; ?>" method="post">
            <p>Subject name:
                <input type="text" name="menu_name" value="<?php echo $currentSubject['menu_name']; ?>" />
            </p>
            <p>Position:
                <select name="position">
                    <?php
                    $subjectsCount = getSubjectsCount($connection);
                    for ($count = 1; $count <= $subjectsCount; $count++) {
                        echo "<option value=\"{$count}\"";
                        if ($currentSubject['position'] == $count) {
                            echo ' selected';
                        }
                        echo ">{$count}</option>";
                    }
                    ?>
                </select>
            </p>
            <p>Visible:
                    <input type="radio" name="visible" value="0"
                        <?php if ($currentSubject[visible] == 0) echo 'checked'; ?>
                    /> No
                    &nbsp;
                    <input type="radio" name="visible" value="1"
                        <?php if ($currentSubject[visible] == 1) echo 'checked'; ?>
                    /> Yes
            </p>
            <input type="submit" value="Edit Subject" name="submit"/>
        </form>
        <br />
        <a href="manage_content.php">Cancel</a>
    </div>
</main>

<?php include('../includes/layouts/footer.php'); ?>
