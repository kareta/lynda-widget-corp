<?php require_once('../includes/session.php'); ?>
<?php require_once('../includes/db_connection.php'); ?>
<?php require_once('../includes/functions.php'); ?>
<?php include('../includes/layouts/header.php'); ?>

<?php
$currentSubject = null;
$currentPage = null;
findSelectedPage($connection, $currentSubject, $currentPage);
?>

<main>
    <nav class="subjects">
        <?php echo renderNavigation($connection, $currentSubject, $currentPage); ?>
        <br />
        <a href="new_subject.php">+ Add a subject</a>
    </nav>
    <div id="page">
        <?php echo message();?>

        <?php if ($currentSubject) {?>
            <h2>Manage Subject</h2>
            Menu name: <?php echo $currentSubject['menu_name']; ?>
            <br/>
            <a href="edit_subject.php?subject=<?php echo $currentSubject['id']; ?>">
                edit the subject
            </a>
        <?php } elseif ($currentPage) { ?>
            <h2>Manage Page</h2>
            Menu name: <?php echo $currentPage['menu_name']; ?>
        <?php } else { ?>
            <h2>Manage Content</h2>
            Please select a subject or a page.
        <?php } ?>

        <br/>
    </div>
</main>

<?php include('../includes/layouts/footer.php'); ?>
