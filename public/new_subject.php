<?php require_once('../includes/session.php'); ?>
<?php require_once('../includes/db_connection.php'); ?>
<?php require_once('../includes/functions.php'); ?>
<?php include('../includes/layouts/header.php'); ?>

<?php
$currentSubject = null;
$currentPage = null;
findSelectedPage($connection, $currentSubject, $currentPage);
?>

<main>
    <nav class="subjects">
        <?php echo renderNavigation($connection, $currentSubject, $currentPage); ?>
    </nav>
    <div id="page">
        <?php echo message(); ?>
        <?php $errors = errors(); ?>
        <?php echo formErrors($errors); ?>
        <h2>Create Subject</h2>
        <form action="create_subject.php" method="post">
            <p>Subject name:
                <input type="text" name="menu_name" value="" />
            </p>
            <p>Position:
                <select name="position">
                    <?php
                    $subjectsCount = getSubjectsCount($connection);
                    for ($count = 1; $count <= $subjectsCount; $count++) {
                        echo "<option value=\"{$count}\">{$count}</option>";
                    }
                    ?>
                </select>
            </p>
            <p>Visible:
                <input type="radio" name="visible" value="0" /> No
                &nbsp;
                <input type="radio" name="visible" value="1" /> Yes
            </p>
            <input type="submit" value="Create Subject" name="submit"/>
        </form>
        <br />
        <a href="manage_content.php">Cancel</a>
    </div>
</main>

<?php include('../includes/layouts/footer.php'); ?>
